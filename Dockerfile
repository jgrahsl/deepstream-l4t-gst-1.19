FROM nvcr.io/nvidia/deepstream-l4t:5.0.1-20.09-samples

RUN apt-get update

RUN apt-get install -y \
	git python3-pip flex bison build-essential openssl cmake pkg-config libgirepository1.0-dev ninja-build libjpeg-dev libssl-dev libnice-dev libpng-dev libglib2.0-dev libcairo-gobject2 \
	locales locales-all

RUN apt-get purge -y gstreamer1.0-alsa gstreamer1.0-libav gstreamer1.0-plugins-bad gstreamer1.0-plugins-base gstreamer1.0-plugins-good gstreamer1.0-plugins-ugly gstreamer1.0-rtsp gstreamer1.0-tools libgstreamer1.0-0 libgstreamer-gl1.0-0 libgstreamer-plugins-bad1.0-0 libgstreamer-plugins-base1.0-0  libgstreamer-plugins-good1.0-0 libgstrtspserver-1.0-0 libnice10

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

RUN pip3 install meson

RUN mkdir /work
WORKDIR /work
RUN git clone https://github.com/GStreamer/gst-build.git
WORKDIR /work/gst-build
RUN /usr/local/bin/meson -Dpython=enabled -Dsharp=disabled -Drtsp_server=disabled -Dges=disabled -Dlibav=disabled build
RUN cd build && ninja -j 8 && ninja install

ENV GST_PLUGIN_PATH=/usr/local/lib/aarch64-linux-gnu/gstreamer-1.0
ENV GST_PLUGIN_PATH=$GST_PLUGIN_PATH:/usr/lib/aarch64-linux-gnu/gstreamer-1.0/
ENV GST_PLUGIN_PATH=$GST_PLUGIN_PATH:/opt/nvidia/deepstream/deepstream-5.0/lib/gst-plugins/

ENV GI_TYPELIB_PATH=/usr/local/lib/aarch64-linux-gnu/girepository-1.0


ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/aarch64-linux-gnu
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib/aarch64-linux-gnu
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/opt/nvidia/deepstream/deepstream-5.0/lib

WORKDIR /opt/nvidia/deepstream/deepstream-5.0/lib
RUN python3 setup.py install

RUN ldconfig

WORKDIR /root
RUN git clone https://gitlab.com/jgrahsl/deepstream-poc.git

WORKDIR /root/deepstream-poc/

COPY test.sh /

