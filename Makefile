build:
	docker build . -t myds
run:
	docker run --rm --privileged -it myds python3 deepstream_test_1_usb.py /dev/video0
shell:
	docker run --rm --privileged -it myds /bin/bash
test:
	docker run --rm --privileged -it myds /bin/bash /test.sh

