#!/bin/bash

set -e

echo "Test for DS Infer gst plugin ..."
gst-inspect-1.0 | grep nvdsgst_infer

echo "Test for DS OSD gst plugin ..."
gst-inspect-1.0 | grep nvds | grep osd

echo "Test for WebRTC plugin ..."
gst-inspect-1.0  | grep webrtcbin

echo "Test for blacklisted plugins ..."
echo "Blacklisted: " `gst-inspect-1.0 -b | grep Total | cut -d' ' -f 3`
